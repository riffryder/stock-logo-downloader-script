# IEX Cloud Stock Logo and CSV Downloader

**Tools/Technologies Used**

* [Axios](https://www.npmjs.com/package/axios)
* [Download-File](https://www.npmjs.com/package/download-file)
* [CSV-Writer](https://www.npmjs.com/package/csv-writer)
* [Dotenv](https://www.npmjs.com/package/dotenv)

# Setup

You will to need to register for a token at the IEX Cloud API(https://iexcloud.io/) and add it into a .env file if using Dotenv.


Run:

```sh
$ npm install

```

Create a .env in the root directory with the following variables:

```sh
IEX_API_KEY=token
```

Create an assets direct in the project's root directory. And inside it, create another directory called csv-files-and-logos.
Within this directory, create 9 different directories named batch-1, batch-2... batch-9

Then run the script using node:

```sh
$ node script.js
```