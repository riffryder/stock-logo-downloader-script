require('dotenv').config()

const axios = require('axios')
const createCsvWriter = require('csv-writer').createArrayCsvWriter;
const download = require('download-file');

const IEX_API_KEY = process.env.IEX_API_KEY;

//INITIAL FETCH OF STOCK SYMBOLS

let stockSymbols = [];

axios.get(`https://cloud.iexapis.com/beta/ref-data/symbols?token=${IEX_API_KEY}`)
  .then((data) => {
    // console.log('Stock symbol data received', data.data);
    stockSymbols = data.data;
    // console.log('These are the stock symbols', stockSymbols);
    return fetchStockData(stockSymbols);
  })
  .catch((error) => {
    console.log('Error fetching stock symbol data', error);
  });

//FETCH STOCK DATA AND MARKET CAPS 

let tickersAndMarketCaps = [];

let fetchStockData = (stockSymbols) => {
  let firstBatch = stockSymbols.slice(0, 1000);
  let secondBatch = stockSymbols.slice(1000, 2000);
  let thirdBatch = stockSymbols.slice(2000, 3000);
  let fourthBatch = stockSymbols.slice(3000, 4000);
  let fifthBatch = stockSymbols.slice(4000, 50000);
  let sixthBatch = stockSymbols.slice(5000, 6000);
  let seventhBatch = stockSymbols.slice(6000, 7000);
  let eigthBatch = stockSymbols.slice(7000, 8000);
  let ninthBatch = stockSymbols.slice(8000);

  let batches = [
    firstBatch, 
    secondBatch, 
    thirdBatch, 
    fourthBatch, 
    fifthBatch, 
    sixthBatch, 
    seventhBatch, 
    eigthBatch,
    ninthBatch
  ]

  // console.log('THIS THE FIRST BATCH', batches[0]);

  let fetchRecursive = (indexA = 0, indexB = 0) => {

    if (!batches[indexA][indexB].symbol) {
      sortStocksByMarketCapAndWriteToCSV(tickersAndMarketCaps, indexA);
      fetchRecursive(indexA += 1);
      tickersAndMarketCaps = [];
    }

    let ticker = batches[indexA][indexB].symbol;
    let requestUrl = `https://cloud.iexapis.com/beta/stock/${ticker}/quote?token=${IEX_API_KEY}`;

    axios.get(requestUrl)
      .then((result) => {
        console.log('This is the result from fecth stock data' , result.data.symbol, result.data.marketCap, indexA, indexB);

        let stockData = {};
        stockData.symbol = result.data.symbol;
        stockData.marketCap = result.data.marketCap;
        tickersAndMarketCaps.push(stockData);
        fetchRecursive(indexA, indexB += 1);

      })
      .catch((error) => {
        console.log('Error fetching stock data', error);
        sortStocksByMarketCapAndWriteToCSV(tickersAndMarketCaps, indexA);
        tickersAndMarketCaps = [];

        fetchRecursive(indexA += 1);
      })
  }

  fetchRecursive();
};

//FILTER STOCKS BY MARKET CAP AND SAVE TO CSV FILE

let sortStocksByMarketCapAndWriteToCSV = (stocks, index) => {
  // console.log("THESE ARE THE STOCKS TO WRITE", stocks);
  let stocksHash = [];
  
  const csvWriter = createCsvWriter({
    path: `./assets/csv-files-and-logos/batch-${index + 1}/stocksbyMarketCap-batch-${index + 1}.csv`,
    header: [
      'TICKER',
      'MARKET CAP'
    ]
  });

  // console.log('These are the stocks to sort', stocks);
  
  for (let stock of stocks) {
    let hash = Object.values(stock);
    
    stocksHash.push(hash);
    stocksHash = stocksHash.sort((a, b) => { return b[1] - a[1] });
  }

  if (stocksHash.length > 500) {
    fetchAndDownloadStockLogos(stocksHash, index);
  }
  

  // console.log('This is the stocksHash and index', stocksHash, index)
  
  
  csvWriter.writeRecords(stocksHash)
    .then(() => {
      console.log('CSV successfully created... Check project root folder');
    })
    .catch((error) => {
      console.log('Error writing CSV file', error);
    })
};

//FETCH AND DOWNLOAD LOGOS FOR STOCK TICKERS

let fetchAndDownloadStockLogos = (stocks, fileIndex) => {
  
  let fetchRecursive = (index = 0) => {

    if (!stocks[index]) {
      return;
    }
    
    // console.log('This is the stocks logos', stocks, index);
    let ticker = stocks[index][0];
    let requestUrl = `https://cloud.iexapis.com/beta/stock/${ticker}/logo?token=${IEX_API_KEY}`

    axios.get(requestUrl)
      .then((result) => {
        let logoUrl = result.data.url;

        let options = {
          directory: `./assets/csv-files-and-logos/batch-${fileIndex + 1}/`,
          filename: `${ticker}.png`
        }

        download(logoUrl, options, (err) => {
          if (err) {
            console.log("Error downloading logo", err.data);
          }
          console.log("Sucessfully downloaded logo", index)
          fetchRecursive(index += 1);
        });

      })

      .catch((error) => {
        console.log('Error fetching logos', error);

        fetchRecursive(index += 1);
      })
  }

  fetchRecursive();
}